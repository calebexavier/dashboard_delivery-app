<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('BASE_DIR', '/dashboard_delivery-app/dashboard_delivery-app/');
require_once './Classes/urlClass.php';
require_once './Classes/Routes.php';

$router = new Routes(UrlClass::getURL(0), UrlClass::getURL(1));

echo $router->content();
