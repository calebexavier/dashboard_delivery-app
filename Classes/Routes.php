<?php


class Routes
{
    private $url;
    private $content;

    public function __construct($url, $id)
    {
        $this->url = $url;
        $this->id = $id;
        $this->getRotaInfo();
    }

    public function content()
    {
        return $this->content;
    }

    private function getRotaInfo()
    {
        $rootPage = __DIR__ . '/../pages';
        ob_start();
        if ($this->url !== 'login') {
            if (@$_GET['admin'] == true) {
                require './partials/header_admin.php';
            } else {
                require './partials/header.php';
            }
        }
        switch ($this->url) {
          case 'login':
            echo '
            <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Login/css/login.css" media="screen" />
            <script type="module" src="'.BASE_DIR.'./pages/Login/js/login.js"></script>
            ';
            require $rootPage . '/Login/view/login.php';
          break;

            case 'home':
              echo '
              <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Principal/css/principal.css" media="screen" />
              <script type="module" src="'.BASE_DIR.'./pages/Principal/js/principal.js"></script>
              ';
              require $rootPage . '/Principal/view/principal.php';
            break;

            case 'home_admin':
              echo '
                <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Principal_Admin/css/principal_admin.css" media="screen" />
                <script type="module" src="'.BASE_DIR.'./pages/Principal_Admin/js/principal_admin.js"></script>
                ';
                require $rootPage . '/Principal_Admin/view/principal_admin.php';
            break;

            case 'alterar_dados':
                echo '
                <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Alterar_Dados/css/alterar_dados.css" media="screen" />
                <script type="module" src="'.BASE_DIR.'./pages/Alterar_Dados/js/alterar_dados.js"></script>
                ';
                require $rootPage . '/Alterar_Dados/view/alterar_dados.php';
            break;

            case 'alterar_dados_admin':
              echo '
              <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Alterar_Dados_Admin/css/alterar_dados_admin.css" media="screen" />
              <script type="module" src="'.BASE_DIR.'./pages/Alterar_Dados_Admin/js/alterar_dados_admin.js"></script>
              ';
              require $rootPage . '/Alterar_Dados_Admin/view/alterar_dados_admin.php';
            break;

            case 'alterar_senha':
              echo '
              <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Alterar_Senha/css/alterar_senha.css" media="screen" />
              <script type="module" src="'.BASE_DIR.'./pages/Alterar_Senha/js/alterar_senha.js"></script>
              ';
              require $rootPage . '/Alterar_Senha/view/alterar_senha.php';
            break;

            case 'alterar_senha_admin':
              echo '
              <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Alterar_Senha_Admin/css/alterar_senha_admin.css" media="screen" />
              <script type="module" src="'.BASE_DIR.'./pages/Alterar_Senha_Admin/js/alterar_senha_admin.js"></script>
              ';
              require $rootPage . '/Alterar_Senha_Admin/view/alterar_senha_admin.php';
            break;

            case 'chat_suporte':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Chat_Suporte/css/chat_suporte.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Chat_Suporte/js/chat_suporte.js"></script>
                  ';
                  require $rootPage . '/Chat_Suporte/view/chat_suporte.php';
            break;

            case 'chat_entregador':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Chat_Entregador/css/chat_entregador.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Chat_Entregador/js/chat_entregador.js"></script>
                  ';
                  require $rootPage . '/Chat_Entregador/view/chat_entregador.php';
            break;

            case 'chat_cliente':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Chat_Cliente/css/chat_cliente.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Chat_Cliente/js/chat_cliente.js"></script>
                  ';
                  require $rootPage . '/Chat_Cliente/view/chat_cliente.php';
            break;

            case 'editar_estabelecimento':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Editar_Estabelecimento/css/editar_estabelecimento.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Editar_Estabelecimento/js/editar_estabelecimento.js"></script>
                  ';
                  require $rootPage . '/Editar_Estabelecimento/view/editar_estabelecimento.php';
            break;
            case 'listar_estabelecimento':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Listar_Estabelecimento/css/listar_estabelecimento.css" media="screen" />
                  <script type="module/javascript" src="'.BASE_DIR.'./pages/Listar_Estabelecimento/js/listar_estabelecimento.js"></script>
                  ';
                  require $rootPage . '/Listar_Estabelecimento/view/listar_estabelecimento.php';
            break;

            case 'solicitacoes':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Solicitacoes/css/solicitacoes.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Solicitacoes/js/solicitacoes.js"></script>
                  ';
                  require $rootPage . '/Solicitacoes/view/solicitacoes.php';
            break;

            case 'pedidos_parceria':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Pedidos_Parceria/css/pedidos_parceria.css" media="screen"/>
                  <script type="module" src="'.BASE_DIR.'./pages/Pedidos_Parceria/js/pedidos_parceria.js"></script>
                  ';
                  require $rootPage . '/Pedidos_Parceria/view/pedidos_parceria.php';
            break;

            case 'listagem':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Listagem/css/listagem.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Listagem/js/listagem.js"></script>
                  ';
                  require $rootPage . '/Listagem/view/listagem.php';
            break;

            case 'dados_entregador':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Dados_Cadastrais_Entregador/css/dados_cadastrais_entregador.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Dados_Cadastrais_Entregador/js/dados_cadastrais_entregador.js"></script>
                  ';
                  require $rootPage . '/Dados_Cadastrais_Entregador/view/dados_cadastrais_entregador.php';
            break;

            case 'identidade':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Identidade/css/identidade.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Identidade/js/identidade.js"></script>
                  ';
                  require $rootPage . '/Identidade/view/identidade.php';
            break;

            case 'cnh':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Cnh/css/cnh.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Cnh/js/cnh.js"></script>
                  ';
                  require $rootPage . '/Cnh/view/cnh.php';
            break;

            case 'perfil':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Perfil/css/perfil.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Perfil/js/perfil.js"></script>
                  ';
                  require $rootPage . '/Perfil/view/perfil.php';
            break;

            case 'categorias':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Categorias/css/categorias.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Categorias/js/categorias.js"></script>
                  ';
                  require $rootPage . '/Categorias/view/categorias.php';
            break;

            case 'editar_categorias':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Editar_Categorias/css/editar_categorias.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Editar_Categorias/js/editar_categorias.js"></script>
                  ';
                  require $rootPage . '/Editar_Categorias/view/editar_categorias.php';
            break;
            case 'listar_categorias':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Listar_Categorias/css/listar_categorias.css" media="screen" />
                  <script type="module/javascript" src="'.BASE_DIR.'./pages/Listar_Categorias/js/listar_categorias.js"></script>
                  ';
                  require $rootPage . '/Listar_Categorias/view/listar_categorias.php';
            break;
            case 'produtos':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Produtos/css/produtos.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Produtos/js/produtos.js"></script>
                  ';
                  require $rootPage . '/Produtos/view/produtos.php';
            break;
            case 'editar_produtos':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Editar_Produtos/css/editar_produtos.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Editar_Produtos/js/editar_produtos.js"></script>
                  ';
                  require $rootPage . '/Editar_Produtos/view/editar_produtos.php';
            break;
            case 'listar_produtos':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Listar_Produtos/css/listar_produtos.css" media="screen" />
                  <script type="module/javascript" src="'.BASE_DIR.'./pages/Listar_Produtos/js/listar_produtos.js"></script>
                  ';
                  require $rootPage . '/Listar_Produtos/view/listar_produtos.php';
            break;
            case 'editar_ingredientes':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Editar_Ingredientes/css/editar_ingredientes.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Editar_Ingredientes/js/editar_ingredientes.js"></script>
                  ';
                  require $rootPage . '/Editar_Ingredientes/view/editar_ingredientes.php';
            break;
            case 'vendas_mes':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Vendas_Mes/css/vendas_mes.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Vendas_Mes/js/vendas_mes.js"></script>
                  ';
                  require $rootPage . '/Vendas_Mes/view/vendas_mes.php';
            break;
            case 'vendas_dia':
              echo '
                  <link rel="stylesheet" type="text/css" href="'.BASE_DIR.'./pages/Vendas_Dia/css/vendas_dia.css" media="screen" />
                  <script type="module" src="'.BASE_DIR.'./pages/Vendas_Dia/js/vendas_dia.js"></script>
                  ';
                  require $rootPage . '/Vendas_Dia/view/vendas_dia.php';
            break;
        }

        if ($this->url !== 'login') {
            require './partials/footer.php';
        }
        $this->content = ob_get_clean();
        return $this;
    }
}

