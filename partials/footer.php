    </div>
  </section>
</div>
  <!-- /.content-wrapper -->
<footer class="main-footer footer-expand">
    <strong>Copyright &copy; 2020 <a href="https://everso.com.br/#overlappable-3">Everso </a>.</strong>
    Todos os direitos reservados.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
</footer>

<!--Char JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!--jQuery Mask-->
<script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>
<!--jQuery Mask
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>-->
<!-- jQuery UI 1.11.4 -->
<script src="<?= BASE_DIR ?>./plugins/jquery-ui/jquery-ui.min.js"></script>
<!--Data Tables Jquery-->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?= BASE_DIR ?>./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= BASE_DIR ?>./dist/js/adminlte.js"></script>
</body>
</html>
