<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Rocket Delivery | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--Favicon Icon-->
  <link rel="icon" type="imagem/png" href="<?= BASE_DIR ?>./dist/img/RocketLogo.png" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= BASE_DIR ?>./plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= BASE_DIR ?>./plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= BASE_DIR ?>./plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= BASE_DIR ?>./plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= BASE_DIR ?>./dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= BASE_DIR ?>./plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= BASE_DIR ?>./plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= BASE_DIR ?>./plugins/summernote/summernote-bs4.css">
  <!--Data Tables-->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-light" style="background-color: #EB4B4D;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= BASE_DIR ?>./home_admin" class="nav-link">Inicio</a>
      </li>
    </ul>

    <!-- SEARCH FORM 
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Pesquisar..." aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>-->

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-primary navbar-badge">1</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="<?= BASE_DIR ?>./chat_cliente" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Humberto  
                  <h5 class ="pt-1" style="font-size: 0.75rem !important;color: grey !important;"> 
                    Cliente Novo!
                  </h5>
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Boa Tarde, tudo ok ?</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> Há 17 minutos</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?= BASE_DIR ?>./chat_cliente" class="dropdown-item dropdown-footer">Veja todas as mensagens</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-primary navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">3 Notificações</span>
          <div class="dropdown-divider"></div>
          <a href="<?= BASE_DIR ?>./chat_cliente" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 1 nova mensagem
            <span class="float-right text-muted text-sm">há 17 minutos</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?= BASE_DIR ?>./home" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 2 novos pedidos
            <span class="float-right text-muted text-sm">há 30 minutos</span>
          </a>
          <div class="dropdown-divider"></div>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?= BASE_DIR ?>./dist/img/RocketLogo.png" alt="Rocket Logo" class="brand-image img-circle elevation-3">
      <span class="brand-text font-weight-light">Rocket Delivery</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Administrador</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview pt-3">    
            <a href="<?= BASE_DIR ?>./home_admin" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Inicio
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview pt-3">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-tie"></i>
              <p>
                Meu Perfil
                <i class="fas fa-angle-left right"></i>
                <!-- <span class="badge badge-info right">6</span> -->
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= BASE_DIR ?>./#" class="nav-link">
                  <i class="nav-icon fas fa-user-edit"></i>
                  <p>Alterar Dados</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= BASE_DIR ?>./#" class="nav-link">
                  <i class="nav-icon fas fa-eye"></i>
                  <p>Alterar Senha</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview pt-3">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-comments"></i>
              <p>
                Chat
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= BASE_DIR ?>./#" class="nav-link">
                  <i class="nav-icon fas fa-comment-dots"></i>
                  <p>Estabelecimento</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= BASE_DIR ?>./#" class="nav-link">
                  <i class="nav-icon far fa-comment-dots"></i>
                  <p>Entregador</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview pt-3">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-store"></i>
              <p>
                Estabelecimento
                <i class="fas fa-angle-left right"></i>
                <!-- <span class="badge badge-info right">6</span> -->
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= BASE_DIR ?>./editar_estabelecimento" class="nav-link">
                  <i class="nav-icon fas fa-cash-register"></i>
                  <p>Cadastrar</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= BASE_DIR ?>./listar_estabelecimento" class="nav-link">
                  <i class="nav-icon fas fa-list"></i>
                  <p>Listar</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview pt-3">
            <a href="#" class="nav-link">
            <i class="nav-icon fas fa-motorcycle"></i>
              <p>
                Entregador
                <i class="fas fa-angle-left right"></i>
                <!-- <span class="badge badge-info right">6</span> -->
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= BASE_DIR ?>./solicitacoes" class="nav-link">
                  <i class="nav-icon fas fa-chart-pie"></i>
                  <p>Solicitações</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= BASE_DIR ?>./listagem" class="nav-link">
                  <i class="nav-icon fas fa-chart-bar"></i>
                  <p>Listagem</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <nav class="mt-3">
        <ul class="nav nav-pills nav-sidebar flex-column d-flex align-items-center" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item pt-3">
              <a class="nav-link" href="<?= BASE_DIR ?>./login">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>Sair</p>
              </a>
            </li>
        </ul> 
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
