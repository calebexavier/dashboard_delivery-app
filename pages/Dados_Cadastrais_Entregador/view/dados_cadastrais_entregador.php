<div class="wrapper col-sm-4 col-lg-12 px-2 ">
    <div class="card-body px-auto rounded">
        <div class="col-12 col-sm-12 col-lg-12 ">
          <div class="card px-1">
            <span class="card-body">
                <h1 class="title">Dados Cadastrais</h1>
                <hr class="border-top mb-3"/>
                <br>
                <form>
                <!--DADOS PESSOAIS-->
            <div class="form-row"><!--TODOS OS CAMPOS DEVEM SER DESABILITADOS, AS INFORMAÇÕES SERAO ALIMENTADAS VIA API-->
              <div class="col-sm-6 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Nome Completo</span>
                  <input type="text" class="form-control" placeholder="João das Neves Almeida de Oliveira" disabled>
                </div>
              </div>
              <div class="col-sm-6 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Data de Nascimento</span>
                  <input type="text" class="form-control" placeholder="18/07/1990" disabled>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-sm-5 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">RG</span>
                  <input type="text" class="form-control" placeholder="6137909" disabled>
                </div>
              </div>
              <div class="col-sm-5 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">CPF</span>
                  <input type="text" id="CPF" class="form-control" placeholder="048.665.312-98" disabled>
                </div>
              </div>
              <div class="col-sm-2 py-2">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">
                      <a href="<?= BASE_DIR ?>./perfil">
                        <i class="fas fa-camera"></i>
                        Perfil
                      </a>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-sm-5 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">
                    <i class="far fa-envelope"></i>
                  </span>
                  <input type="text" id="email" class="form-control" placeholder="joaoneves@gmail.com" disabled>
                </div>
              </div>
              <div class="col-sm-5 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Celular</span>
                  <input type="tel" id="telefone-1" class="form-control" placeholder="(62) 9 8615-4875" disabled>
                </div>
              </div>
              <div class="col-sm-2 py-2">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">
                      <a href="<?= BASE_DIR ?>./identidade">
                        <i class="fas fa-camera mr-2"></i>
                        Identidade
                      </a>
                    </span>
                  </div> <!--Icone ao clicar abre imagens da identidade previamente enviadas pelo aspirante a entregador-->
                </div>
              </div>
            </div>

            <h1 class="title mt-5">Veículo</h1> <!--PLACA E DOCUMENTO DO VEICULO(FOTO)--->
            <hr class="border-top mb-3"/>
            <br>
            <div class="form-row">
              <div class="col-sm-4 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Categoria</span>
                  <input type="text" class="form-control" placeholder="Motocicleta" disabled>
                </div>
              </div>
              <div class="col-sm-3 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Placa</span>
                  <input type="text" class="form-control" placeholder="NKM-8156" disabled>
                </div>
              </div>
              <div class="col-sm-3 py-2">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">
                      <a href="<?= BASE_DIR ?>./documento_veiculo">
                        <i class="fas fa-camera mr-2"></i>
                        Documento do Veículo
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-sm-2 py-2">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">
                      <a href="<?= BASE_DIR ?>./cnh">
                        <i class="fas fa-camera mr-2"></i>
                        CNH
                      </a>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            
            <!--CONTA BANCARIA-->
            <h1 class="title mt-5">Conta Bancária</h1>
            <hr class="border-top mb-3"/>
            <br>
            <div class="form-row"><!--TODOS OS CAMPOS DEVEM SER DESABILITADOS, AS INFORMAÇÕES SERAO ALIMENTADAS VIA API-->
              <div class="col-sm-4 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Banco</span>
                  <input type="text" class="form-control" placeholder="260 - NU PAGAMENTOS S.A. " disabled>
                </div>
              </div>
              <div class="col-sm-2 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Agência</span>
                  <input type="text" class="form-control" placeholder="0001" disabled>
                </div>
              </div>
              <div class="col-sm-4 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">conta bancária</span>
                  <input type="text" class="form-control" placeholder="07854259" disabled>
                </div>
              </div>
              <div class="col-sm-2 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Dígito</span>
                  <input type="text" class="form-control" placeholder="1" disabled>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col mt-5 d-flex justify-content-center">
                <a href="<?= BASE_DIR ?>./#">
                  <div id="btn-salvar" class="input-group-prepend">
                    <button type="button" class="btn btn-danger float-right">Excluir</button> <!--Vermelho-->
                  </div>
                </a>
              </div>
            </div>
          </form>
        </span>
      </div>
    </div>
  </div>
</div>
