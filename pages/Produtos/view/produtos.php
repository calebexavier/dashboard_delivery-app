<div class="col-sm-4 col-lg-12 px-2 ">
  <div class="card-body px-auto rounded">
    <div class="col-12 col-sm-12 col-lg-12 ">
      <div class="card px-1">
        <span class="card-body">
          <h1 class="title">Produtos</h1>
          <hr class="border-top mb-3"/>
          <br>
          <div class="row">
            <div class="col-sm-6">
              <div class="jumbotron d-flex justify-content-center">
                <a class="btn btn-lg" href="<?= BASE_DIR ?>./editar_produtos" role="button">
                  <p class="text-justify">Cadastrar Produtos </p>
                </a>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="jumbotron d-flex justify-content-center">
                <a class="btn btn-lg" href="<?= BASE_DIR ?>./listar_produtos" role="button">
                  <p class="text-justify">Listar Produtos </p>
                </a>
              </div>
            </div>
          </div>
        </span>
      </div>
    </div>
  </div>
</div>
