<section class="content">
  <div class="container-fluid">
    <div class="row mt-5">
      <section class="col-lg-12"><!--Inicio Seção em Andamento-->
        <div class="card">
          <div class="card-header top-red">
            <h3 class="card-title">
              <i class="fas fa-shopping-bag mr-1 icon-white"></i>
              Pedidos em Andamento
            </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus icon-white"></i>
              </button>
            </div>
          </div><!-- /.card-header -->
          <div class="card-body">
            <div class="col-sm-12 col-lg-12">
              <div class="card pt-2"> <!--CARD 1-->
                <a class="btn btn-lg" data-toggle="collapse" href="#Example1" role="button" aria-expanded="false" aria-controls="collapseExample">
                  <div class="pedidos_card">
                    <div class="card-header">
                      <div class="form-row mx-3">
                        <label class="title"> Estabelecimento:</label> <h5 class ="subtitle pt-1 pl-2"> Hamburgueria Gerson</h5><br>
                      </div>
                      <div class="form-row mx-3">
                        <label class="title"> Entregador:</label> <h5 class ="subtitle pt-1 pl-2"> Rodrigo das Neves</h5><br>
                      </div>
                    </div>
                  </div>
                </a>
                <div class="collapse" id="Example1">
                  <div class="card-body d-flex justify-content-center">
                      <div class="col-sm-12">
                        <div class="row d-flex justify-content-center">
                          <div class="col-sm-8">
                            <div class="row d-flex justify-content-center border-bottom">
                              <div class="col pl-0">
                                <br>
                                <p class="subtitle"> Pedido #002</p>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-row ml-4 mt-2"><i class="fas fa-circle ajust"></i><label class="title"> 11:00 - Estabelecimento aceitou o pedido</label><br></div>
                            <div class="form-row ml-4"><i class="fas fa-circle ajust"></i><label class="title"> 11:10 - Pedido em preparo</label><br></div>
                            <div class="form-row ml-4"><i class="fas fa-circle ajust"></i><label class="title"> 11:31 - Entregador saiu para entrega</label><br></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="card pt-2"> <!--CARD 2-->
                <a class="btn btn-lg" data-toggle="collapse" href="#Example2" role="button" aria-expanded="false" aria-controls="collapseExample">
                  <div class="pedidos_card">
                    <div class="card-header">
                      <div class="form-row mx-3">
                        <label class="title"> Estabelecimento:</label> <h5 class ="subtitle pt-1 pl-2"> Hamburgueria Gerson</h5><br>
                      </div>
                      <div class="form-row mx-3">
                        <label class="title"> Entregador:</label> <h5 class ="subtitle pt-1 pl-2"> Rodrigo das Neves</h5><br>
                      </div>
                    </div>
                  </div>
                </a>
                <div class="collapse" id="Example2">
                  <div class="card-body d-flex justify-content-center">
                      <div class="col-sm-12">
                        <div class="row d-flex justify-content-center">
                          <div class="col-sm-8">
                            <div class="row d-flex justify-content-center border-bottom">
                              <div class="col pl-0">
                                <br>
                                <p class="subtitle"> Pedido #002</p>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-row ml-4 mt-2"><i class="fas fa-circle ajust"></i><label class="title"> 11:00 - Estabelecimento aceitou o pedido</label><br></div>
                            <div class="form-row ml-4"><i class="fas fa-circle ajust"></i><label class="title"> 11:10 - Pedido em preparo</label><br></div>
                            <div class="form-row ml-4"><i class="fas fa-circle ajust"></i><label class="title"> 11:31 - Entregador saiu para entrega</label><br></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="card pt-2"> <!--CARD 3-->
                <a class="btn btn-lg" data-toggle="collapse" href="#Example3" role="button" aria-expanded="false" aria-controls="collapseExample">
                  <div class="pedidos_card">
                    <div class="card-header">
                      <div class="form-row mx-3">
                        <label class="title"> Estabelecimento:</label> <h5 class ="subtitle pt-1 pl-2"> Hamburgueria Gerson</h5><br>
                      </div>
                      <div class="form-row mx-3">
                        <label class="title"> Entregador:</label> <h5 class ="subtitle pt-1 pl-2"> Rodrigo das Neves</h5><br>
                      </div>
                    </div>
                  </div>
                </a>
                <div class="collapse" id="Example3">
                  <div class="card-body d-flex justify-content-center">
                      <div class="col-sm-12">
                        <div class="row d-flex justify-content-center">
                          <div class="col-sm-8">
                            <div class="row d-flex justify-content-center border-bottom">
                              <div class="col pl-0">
                                <br>
                                <p class="subtitle"> Pedido #002</p>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-row ml-4 mt-2"><i class="fas fa-circle ajust"></i><label class="title"> 11:00 - Estabelecimento aceitou o pedido</label><br></div>
                            <div class="form-row ml-4"><i class="fas fa-circle ajust"></i><label class="title"> 11:10 - Pedido em preparo</label><br></div>
                            <div class="form-row ml-4"><i class="fas fa-circle ajust"></i><label class="title"> 11:31 - Entregador saiu para entrega</label><br></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="card pt-2"> <!--CARD 4-->
                <a class="btn btn-lg" data-toggle="collapse" href="#Example4" role="button" aria-expanded="false" aria-controls="collapseExample">
                  <div class="pedidos_card">
                    <div class="card-header">
                      <div class="form-row mx-3">
                        <label class="title"> Estabelecimento:</label> <h5 class ="subtitle pt-1 pl-2"> Hamburgueria Gerson</h5><br>
                      </div>
                      <div class="form-row mx-3">
                        <label class="title"> Entregador:</label> <h5 class ="subtitle pt-1 pl-2"> Rodrigo das Neves</h5><br>
                      </div>
                    </div>
                  </div>
                </a>
                <div class="collapse" id="Example4">
                  <div class="card-body d-flex justify-content-center">
                      <div class="col-sm-12">
                        <div class="row d-flex justify-content-center">
                          <div class="col-sm-8">
                            <div class="row d-flex justify-content-center border-bottom">
                              <div class="col pl-0">
                                <br>
                                <p class="subtitle"> Pedido #002</p>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-8">
                            <div class="form-row ml-4 mt-2"><i class="fas fa-circle ajust"></i><label class="title"> 11:00 - Estabelecimento aceitou o pedido</label><br></div>
                            <div class="form-row ml-4"><i class="fas fa-circle ajust"></i><label class="title"> 11:10 - Pedido em preparo</label><br></div>
                            <div class="form-row ml-4"><i class="fas fa-circle ajust"></i><label class="title"> 11:31 - Entregador saiu para entrega</label><br></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </section> <!--Fim-->

      <section class="col-lg-12"><!--Inicio Seção Concluidos-->
        <div class="card">
          <div class="card-header top-green">
            <h3 class="card-title">
              <i class="fas fa-shopping-bag mr-1 icon-white"></i>
              Pedidos Concluídos
            </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus icon-white"></i>
              </button>
            </div>
          </div><!-- /.card-header -->
          <div class="card-body">
            <div class="col-sm-12 col-lg-12">
              <div class="card pt-2"> <!--CARD 1-->
                <a class="btn btn-lg" data-toggle="collapse" href="#CARD1" role="button" aria-expanded="false" aria-controls="collapseExample">
                  <div class="pedidos_card">
                    <div class="card-header">
                      <div class="form-row mx-3">
                        <label class="title"> Estabelecimento:</label> <h5 class ="subtitle pt-1 pl-2"> Hamburgueria Gerson</h5><br>
                      </div>
                      <div class="form-row mx-3">
                        <label class="title"> Entregador:</label> <h5 class ="subtitle pt-1 pl-2"> José Barreto Ribeiro</h5><br>
                      </div>
                    </div>
                  </div>
                </a>
                <div class="collapse" id="CARD1">
                  <div class="card-body d-flex justify-content-center">
                    <div class="col-sm-12">
                      <div class="row d-flex justify-content-center ">
                        <div class="col-sm-8">
                          <div class="row d-flex justify-content-center border-bottom">
                            <div class="col pl-0">
                              <br>
                              <p class="subtitle"> Pedido #001</p>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-8">
                          <div class="form-row ml-4 mt-2"><i class="fas fa-circle ajust"></i><label class="title"> 09:00 - Estabelecimento aceitou o pedido</label><br></div>
                          <div class="form-row ml-4"><i class="fas fa-circle ajust"></i><label class="title"> 09:10 - Pedido em preparo</label><br></div>
                          <div class="form-row ml-4"><i class="fas fa-circle ajust"></i><label class="title"> 10:31 - Entregador saiu para entrega</label><br></div>
                          <div class="form-row ml-4"><i class="fas fa-circle ajust"></i><label class="title"> 10:58 - Pedido Finalizado</label><br></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> <!--Fim-->
