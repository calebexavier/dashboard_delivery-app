
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Atalhos Rápidos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <!--<ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= BASE_DIR ?>./home">Home</a></li>
              <li class="breadcrumb-item active"> Minha página </li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>2</h3>

                <p>Pedidos Abertos</p>
                <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>25 <sup style="font-size: 20px">%</sup></h3>

                <p>Conclusão de Hoje</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>6</h3>

                <p>Clientes novos</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>R$ 1200,00 </h3>

                <p>Receita Diária</p>
              </div>
              <div class="icon">
                <i class="ion ion-cash"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <section class="col-lg-12">
          <!-- Inicio pedidos)-->
            <div class="card">
              <div class="card-header top-red">
                <h3 class="card-title">
                <i class="fas fa-shopping-bag mr-1 icon-white"></i>
                  Pedidos
                </h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus icon-white"></i>
                  </button>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                  <div class="col-sm-12 col-lg-12">
                      <div class="card pt-2">
                          <div class="pedidos_card">
                            <div class="card-header">
                              <div class="row d-flex justify-content-end">
                                <div class="justify-content-end">
                                    <span class="badge badge-pill badge-warning" style="font-size: 1rem !important">Pendente</span>
                                </div>
                              </div>
                              <div class="row">
                                <a class="btn btn-lg" data-toggle="collapse" href="#Example1" role="button" aria-expanded="false" aria-controls="collapseExample">
                                  <p class="text-justify">Pedido #001</p>
                                </a>
                              </div>
                              <label class="text-right"> Entregar até as 12:49 </label>
                            </div>
                          </div> 
                          <div class="collapse" id="Example1">
                            <div class="card-body d-flex justify-content-center">
                              <div class="col-sm-6">
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col pl-0">
                                    <br>
                                    <p class="subtitle"> Pedido #001</p>
                                    <label class="title"> Horário atual 12:20</label>
                                  </div>
                                  <div class="justify-content-end">
                                    <span class="badge badge-pill badge-warning">Pendente</span>
                                    <div class="mt-3">
                                      <a href="#">
                                        <i class="fas fa-print"></i>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col">
                                    <br>
                                    <div class="row"><h1 class="subtitle"> Cliente</h1></div>
                                    <div class="row"><label class="title"> Humberto </label> <h5 class ="subtitle pt-1 pl-2"> Cliente Novo!</h5><br></div>
                                    <div class="row"><a href="#" class="title">062 9888888 </a> <h5 class ="subtitle pt-1 pl-2"> Telefone</h5><br></div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col">
                                    <br>
                                    <div class="row"><h1 class="subtitle"> Itens do Pedido</h1></div>
                                    <div class="row"><label class="title"> Sanduiche xPãozão - R$ 13,99 </label> <br></div>
                                    <div class="row"><h5 class ="subtitle pt-1"> Pão, Queijo Cheddar, salada, Hamburguer, Cebola agridoce e batata.</h5><br></div>
                                    <div class="row"><label class="title"> 1x Pão Francês - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Hambúrguer Bovino - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Tomate - R$ 2,99 </label> <br></div>
                                    <br>
                                    <div class="row"><label class="title"> Sanduiche xPãozão - R$ 13,99 </label> <br></div>
                                    <div class="row"><h5 class ="subtitle pt-1"> Pão, Queijo Cheddar, salada, Hamburguer, Cebola agridoce e batata.</h5><br></div>
                                    <div class="row"><label class="title"> 1x Pão Francês - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Hambúrguer Bovino - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Tomate - R$ 2,99 </label> <br></div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center">
                                      <div class="col">
                                        <br>
                                        <div class="row"><h1 class="subtitle"> Valor do pedido</h1></div>
                                        <div class="row sub">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Sub-total:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title"> R$ 31,96</label>
                                          </div> 
                                        </div>
                                        <div class="row sub">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Frete:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title" > R$ 2,45</label>
                                          </div> 
                                        </div>
                                        <div class="row total">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Total:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title"> R$ 34,41</label>
                                          </div> 
                                        </div>
                                      </div>
                                </div>
                                <br>
                                <br>
                                <div class="row d-flex justify-content-center" class="color: white !important;">
                                  <button class="btn btn-warning btn-lg" type="button" href="#"><b style="color: white !important;">Confirmar </b></button>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="card pt-2">
                          <div class="pedidos_card">
                            <div class="card-header">
                              <div class="row d-flex justify-content-end">
                                <div class="justify-content-end">
                                    <span class="badge badge-pill badge-warning" style="font-size: 1rem !important">Pendente</span>
                                </div>
                              </div>
                              <div class="row">
                                <a class="btn btn-lg" data-toggle="collapse" href="#Example1" role="button" aria-expanded="false" aria-controls="collapseExample">
                                  <p class="text-justify">Pedido #001</p>
                                </a>
                              </div>
                              <label class="text-right"> Entregar até as 12:49 </label>
                            </div>
                          </div> 
                          <div class="collapse" id="Example1">
                            <div class="card-body d-flex justify-content-center">
                              <div class="col-sm-6">
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col pl-0">
                                    <br>
                                    <p class="subtitle"> Pedido #001</p>
                                    <label class="title"> Horário atual 12:20</label>
                                  </div>
                                  <div class="justify-content-end">
                                    <span class="badge badge-pill badge-warning">Pendente</span>
                                    <div class="mt-3">
                                      <a href="#">
                                        <i class="fas fa-print"></i>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col">
                                    <br>
                                    <div class="row"><h1 class="subtitle"> Cliente</h1></div>
                                    <div class="row"><label class="title"> Humberto </label> <h5 class ="subtitle pt-1 pl-2"> Cliente Novo!</h5><br></div>
                                    <div class="row"><a href="#" class="title">062 9888888 </a> <h5 class ="subtitle pt-1 pl-2"> Telefone</h5><br></div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col">
                                    <br>
                                    <div class="row"><h1 class="subtitle"> Itens do Pedido</h1></div>
                                    <div class="row"><label class="title"> Sanduiche xPãozão - R$ 13,99 </label> <br></div>
                                    <div class="row"><h5 class ="subtitle pt-1"> Pão, Queijo Cheddar, salada, Hamburguer, Cebola agridoce e batata.</h5><br></div>
                                    <div class="row"><label class="title"> 1x Pão Francês - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Hambúrguer Bovino - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Tomate - R$ 2,99 </label> <br></div>
                                    <br>
                                    <div class="row"><label class="title"> Sanduiche xPãozão - R$ 13,99 </label> <br></div>
                                    <div class="row"><h5 class ="subtitle pt-1"> Pão, Queijo Cheddar, salada, Hamburguer, Cebola agridoce e batata.</h5><br></div>
                                    <div class="row"><label class="title"> 1x Pão Francês - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Hambúrguer Bovino - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Tomate - R$ 2,99 </label> <br></div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center">
                                      <div class="col">
                                        <br>
                                        <div class="row"><h1 class="subtitle"> Valor do pedido</h1></div>
                                        <div class="row sub">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Sub-total:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title"> R$ 31,96</label>
                                          </div> 
                                        </div>
                                        <div class="row sub">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Frete:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title" > R$ 2,45</label>
                                          </div> 
                                        </div>
                                        <div class="row total">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Total:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title"> R$ 34,41</label>
                                          </div> 
                                        </div>
                                      </div>
                                </div>
                                <br>
                                <br>
                                <div class="row d-flex justify-content-center" class="color: white !important;">
                                  <button class="btn btn-warning btn-lg" type="button" href="#"><b style="color: white !important;">Confirmar </b></button>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="card pt-2">
                          <div class="pedidos_card">
                            <div class="card-header">
                              <div class="row d-flex justify-content-end">
                                <div class="justify-content-end">
                                    <span class="badge badge-pill badge-warning" style="font-size: 1rem !important">Pendente</span>
                                </div>
                              </div>
                              <div class="row">
                                <a class="btn btn-lg" data-toggle="collapse" href="#Example1" role="button" aria-expanded="false" aria-controls="collapseExample">
                                  <p class="text-justify">Pedido #001</p>
                                </a>
                              </div>
                              <label class="text-right"> Entregar até as 12:49 </label>
                            </div>
                          </div> 
                          <div class="collapse" id="Example1">
                            <div class="card-body d-flex justify-content-center">
                              <div class="col-sm-6">
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col pl-0">
                                    <br>
                                    <p class="subtitle"> Pedido #001</p>
                                    <label class="title"> Horário atual 12:20</label>
                                  </div>
                                  <div class="justify-content-end">
                                    <span class="badge badge-pill badge-warning">Pendente</span>
                                    <div class="mt-3">
                                      <a href="#">
                                        <i class="fas fa-print"></i>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col">
                                    <br>
                                    <div class="row"><h1 class="subtitle"> Cliente</h1></div>
                                    <div class="row"><label class="title"> Humberto </label> <h5 class ="subtitle pt-1 pl-2"> Cliente Novo!</h5><br></div>
                                    <div class="row"><a href="#" class="title">062 9888888 </a> <h5 class ="subtitle pt-1 pl-2"> Telefone</h5><br></div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col">
                                    <br>
                                    <div class="row"><h1 class="subtitle"> Itens do Pedido</h1></div>
                                    <div class="row"><label class="title"> Sanduiche xPãozão - R$ 13,99 </label> <br></div>
                                    <div class="row"><h5 class ="subtitle pt-1"> Pão, Queijo Cheddar, salada, Hamburguer, Cebola agridoce e batata.</h5><br></div>
                                    <div class="row"><label class="title"> 1x Pão Francês - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Hambúrguer Bovino - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Tomate - R$ 2,99 </label> <br></div>
                                    <br>
                                    <div class="row"><label class="title"> Sanduiche xPãozão - R$ 13,99 </label> <br></div>
                                    <div class="row"><h5 class ="subtitle pt-1"> Pão, Queijo Cheddar, salada, Hamburguer, Cebola agridoce e batata.</h5><br></div>
                                    <div class="row"><label class="title"> 1x Pão Francês - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Hambúrguer Bovino - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Tomate - R$ 2,99 </label> <br></div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center">
                                      <div class="col">
                                        <br>
                                        <div class="row"><h1 class="subtitle"> Valor do pedido</h1></div>
                                        <div class="row sub">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Sub-total:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title"> R$ 31,96</label>
                                          </div> 
                                        </div>
                                        <div class="row sub">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Frete:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title" > R$ 2,45</label>
                                          </div> 
                                        </div>
                                        <div class="row total">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Total:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title"> R$ 34,41</label>
                                          </div> 
                                        </div>
                                      </div>
                                </div>
                                <br>
                                <br>
                                <div class="row d-flex justify-content-center" class="color: white !important;">
                                  <button class="btn btn-warning btn-lg" type="button" href="#"><b style="color: white !important;">Confirmar </b></button>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="card pt-2">
                          <div class="pedidos_card">
                            <div class="card-header">
                              <div class="row d-flex justify-content-end">
                                <div class="justify-content-end">
                                    <span class="badge badge-pill badge-warning" style="font-size: 1rem !important">Pendente</span>
                                </div>
                              </div>
                              <div class="row">
                                <a class="btn btn-lg" data-toggle="collapse" href="#Example1" role="button" aria-expanded="false" aria-controls="collapseExample">
                                  <p class="text-justify">Pedido #001</p>
                                </a>
                              </div>
                              <label class="text-right"> Entregar até as 12:49 </label>
                            </div>
                          </div> 
                          <div class="collapse" id="Example1">
                            <div class="card-body d-flex justify-content-center">
                              <div class="col-sm-6">
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col pl-0">
                                    <br>
                                    <p class="subtitle"> Pedido #001</p>
                                    <label class="title"> Horário atual 12:20</label>
                                  </div>
                                  <div class="justify-content-end">
                                    <span class="badge badge-pill badge-warning">Pendente</span>
                                    <div class="mt-3">
                                      <a href="#">
                                        <i class="fas fa-print"></i>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col">
                                    <br>
                                    <div class="row"><h1 class="subtitle"> Cliente</h1></div>
                                    <div class="row"><label class="title"> Humberto </label> <h5 class ="subtitle pt-1 pl-2"> Cliente Novo!</h5><br></div>
                                    <div class="row"><a href="#" class="title">062 9888888 </a> <h5 class ="subtitle pt-1 pl-2"> Telefone</h5><br></div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center border-bottom">
                                  <div class="col">
                                    <br>
                                    <div class="row"><h1 class="subtitle"> Itens do Pedido</h1></div>
                                    <div class="row"><label class="title"> Sanduiche xPãozão - R$ 13,99 </label> <br></div>
                                    <div class="row"><h5 class ="subtitle pt-1"> Pão, Queijo Cheddar, salada, Hamburguer, Cebola agridoce e batata.</h5><br></div>
                                    <div class="row"><label class="title"> 1x Pão Francês - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Hambúrguer Bovino - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Tomate - R$ 2,99 </label> <br></div>
                                    <br>
                                    <div class="row"><label class="title"> Sanduiche xPãozão - R$ 13,99 </label> <br></div>
                                    <div class="row"><h5 class ="subtitle pt-1"> Pão, Queijo Cheddar, salada, Hamburguer, Cebola agridoce e batata.</h5><br></div>
                                    <div class="row"><label class="title"> 1x Pão Francês - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Hambúrguer Bovino - Grátis </label> <br></div>
                                    <div class="row"><label class="title"> 2x Tomate - R$ 2,99 </label> <br></div>
                                  </div>
                                </div>
                                <div class="row d-flex justify-content-center">
                                      <div class="col">
                                        <br>
                                        <div class="row"><h1 class="subtitle"> Valor do pedido</h1></div>
                                        <div class="row sub">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Sub-total:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title"> R$ 31,96</label>
                                          </div> 
                                        </div>
                                        <div class="row sub">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Frete:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title" > R$ 2,45</label>
                                          </div> 
                                        </div>
                                        <div class="row total">
                                          <div class="col-sm-6 pl-0">
                                            <label class="title"> Total:  </label>
                                          </div>
                                          <div class="col-sm-6  d-flex justify-content-end">
                                            <label class="title"> R$ 34,41</label>
                                          </div> 
                                        </div>
                                      </div>
                                </div>
                                <br>
                                <br>
                                <div class="row d-flex justify-content-center" class="color: white !important;">
                                  <button class="btn btn-warning btn-lg" type="button" href="#"><b style="color: white !important;">Confirmar </b></button>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                  </div>
              </div><!-- /.card-body -->
            </div>
          </section><!-- /.card -->
