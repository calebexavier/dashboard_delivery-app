<div class="wrapper col-sm-4 col-lg-12 px-2 ">
    <div class="card-body px-auto rounded">

        <div class="col-12 col-sm-12 col-lg-12 ">
          <div class="card px-1">
            <span class="card-body">
                <h1 class="title">Listar Produtos</h1>
                <hr class="border-top mb-3"/>
                <br>
                <br>
                    <!--Campo de pesquisa-->
                        <div class="form-group mx-3">
                            <input class="form-control" id="campo_pesquisa_tabela" type="text" placeholder="Pesquisar por...">
                        </div>
                    <!--Fim do Campo de pesquisa-->

                    <!--Inicio da tabela de Produtos-->
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="bg-danger">
                                    <th class="text-center border-left">Nome do Produto</th>
                                    <th class="text-center border-left">Valor Estimado</th>
                                    <th class="text-center border-left">Status</th>
                                    <th class="text-center border-left">Categoria</th>
                                    <th class="text-center border-left">Função</th>
                                </thead>
                                <tbody id="tabela_categorias_corpo">
                                    <tr>
                                        <td class="text-center">
                                          X-tudo de bom
                                        </td>
                                        <td class="text-center">
                                            R$ 25,00
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                            Sanduíche
                                        </td>
                                        <td class="text-center">
                                          <a class="px-3" href="<?= BASE_DIR ?>./editar_ingredientes" data-toggle="tooltip" data-placement="top" title="Editar Ingredientes">
                                            <i class="fas fa-utensils"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            Cerveja Corona Barril
                                        </td>
                                        <td class="text-center">
                                            R$ 85,00
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                            Bebidas
                                        </td>
                                        <td class="text-center">
                                          <a class="px-3" href="<?= BASE_DIR ?>./editar_ingredientes" data-toggle="tooltip" data-placement="top" title="Editar Ingredientes">
                                            <i class="fas fa-utensils"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            Frango assado inteiro
                                        </td>
                                        <td class="text-center">
                                            R$ 80,00
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                            Assados
                                        </td>
                                        <td class="text-center">
                                          <a class="px-3" href="<?= BASE_DIR ?>./editar_ingredientes" data-toggle="tooltip" data-placement="top" title="Editar Ingredientes">
                                            <i class="fas fa-utensils"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            Arroz cozido na lenha
                                        </td>
                                        <td class="text-center">
                                            R$ 100,00
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                            Caçarolas
                                        </td>
                                        <td class="text-center">
                                          <a class="px-3" href="<?= BASE_DIR ?>./editar_ingredientes" data-toggle="tooltip" data-placement="top" title="Editar Ingredientes">
                                            <i class="fas fa-utensils"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            Batata Frita Mega
                                        </td>
                                        <td class="text-center">
                                            R$ 20,00
                                        </td>
                                        <td class="text-center">
                                            Não Ativo
                                        </td>
                                        <td class="text-center">
                                            Acompanhamentos
                                        </td>
                                        <td class="text-center">
                                          <a class="px-3" href="<?= BASE_DIR ?>./editar_ingredientes" data-toggle="tooltip" data-placement="top" title="Editar Ingredientes">
                                            <i class="fas fa-utensils"></i>
                                          </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    <!--Fim da Tabela de Produtos-->
                </span>
            </div>
        </div>
    </div>
</div>
