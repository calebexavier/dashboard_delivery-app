<div class="wrapper col-sm-4 col-lg-12 px-2 ">
  <div class="card-body px-auto rounded">
    <div class="col-12 col-sm-12 col-lg-12 ">
      <div class="card px-1">
        <span class="card-body">
          <h1 class="title">Perfil</h1>
          <hr class="border-top mb-3"/>
          <br>
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="<?= BASE_DIR ?>./pages/images/perfil.png" class="d-block w-100 img-fluid" alt="Responsive image">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </span>
      </div>
    </div>
  </div>
</div>
