<div class="wrapper col-sm-4 col-lg-12 px-2 ">
    <div class="card-body px-auto rounded">

        <div class="col-12 col-sm-12 col-lg-12 ">
          <div class="card px-1">
            <span class="card-body">
                <h1 class="title">Listar Categorias</h1>
                <hr class="border-top mb-3"/>
                <br>
                <br>
                    <!--Campo de pesquisa-->
                        <div class="form-group mx-3">
                            <input class="form-control" id="campo_pesquisa_tabela" type="text" placeholder="Pesquisar por...">
                        </div>
                    <!--Fim do Campo de pesquisa-->

                    <!--Inicio da tabela de Categorias-->
                        <div class="table-responsive" id="1">
                            <table id="tabela_categorias" class="table table-hover">
                                <thead class="bg-danger">
                                    <th class="text-center ">Código da Categoria</th>
                                    <th class="text-center border-left">Nome da Categoria</th>
                                    <th class="text-center border-left">Status</th>
                                    <th class="text-center border-left">Função</th>
                                </thead>
                                <tbody id="tabela_categorias_corpo">
                                    <tr>
                                        <td class="text-center">
                                            001
                                        </td>
                                        <td class="text-center">
                                            Grelhados
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                          <a class="px-2" href="<?= BASE_DIR ?>./editar_categorias">
                                            <i class="fas fa-edit "></i>
                                          </a>
                                          <a class="px-2" href="#" id="showHideContent" name="showHideContent">
                                            <i class="far fa-eye" id="showHideContent" name="showHideContent"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr id="table_filhos" class="">
                                      <td colspan="4">
                                        <table class="table">
                                          <tbody>
                                            <tr>
                                              <td class="text-center">
                                                  001
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="text-center">
                                                  001
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="text-center">
                                                  001
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="text-center">
                                                  001
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            002
                                        </td>
                                        <td class="text-center">
                                            Porções
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                          <a class="px-2" href="<?= BASE_DIR ?>./editar_categorias">
                                            <i class="fas fa-edit "></i>
                                          </a>
                                          <a class="px-2" href="<?= BASE_DIR ?>./editar_categorias">
                                            <i class="far fa-eye"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            003
                                        </td>
                                        <td class="text-center">
                                            Sanduíche
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                          <a class="px-2" href="<?= BASE_DIR ?>./editar_categorias">
                                            <i class="fas fa-edit "></i>
                                          </a>
                                          <a class="px-2" href="<?= BASE_DIR ?>./editar_categorias">
                                            <i class="far fa-eye"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            004
                                        </td>
                                        <td class="text-center">
                                            Sobremesas
                                        </td>
                                        <td class="text-center">
                                            Não Ativo
                                        </td>
                                        <td class="text-center">
                                          <a class="px-2" href="<?= BASE_DIR ?>./editar_categorias">
                                            <i class="fas fa-edit "></i>
                                          </a>
                                          <a class="px-2" href="<?= BASE_DIR ?>./editar_categorias">
                                            <i class="far fa-eye"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            005
                                        </td>
                                        <td class="text-center">
                                            Bebidas
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                          <a class="px-2 " data-toggle="tooltip" title="Editar" href="<?= BASE_DIR ?>./editar_categorias">
                                            <i class="fas fa-edit "></i>
                                          </a>
                                          <a class="px-2" data-toggle="tooltip" title="Visualizar" href="<?= BASE_DIR ?>./editar_categorias">
                                            <i class="far fa-eye"></i>
                                          </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    <!--Fim da Tabela de Categorias-->
                </span>
            </div>
        </div>
    </div>
</div>
