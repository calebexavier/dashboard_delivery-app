<div class="wrapper col-sm-4 col-lg-12 px-2 ">
    <div class="card-body px-auto rounded">
        <div class="col-12 col-sm-12 col-lg-12 ">
          <div class="card px-1">
            <span class="card-body">
                <h1 class="title">Listagem de Entregadores</h1>
                <hr class="border-top mb-3"/>
                <br>
                    <!--Campo de pesquisa-->
                        <div class="form-group mx-3">
                            <input class="form-control" id="campo_pesquisa_tabela" type="text" placeholder="Pesquisar por...">
                        </div>
                    <!--Fim do Campo de pesquisa-->

                    <!--Inicio da lista de entregadores-->
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="bg-danger">
                                    <th class="text-center border-left">Nome do entregador</th>
                                    <th class="text-center border-left">Função</th>
                                </thead>
                                <tbody id="tabela_categorias_corpo">
                                    <tr>
                                        <td class="text-center">
                                          João Oliveira Rocha Brandos
                                        </td>
                                        <td class="text-center">
                                          <label class="text-justify">
                                            <a class="px-3" href="<?= BASE_DIR ?>./dados_entregador" data-toggle="tooltip" data-placement="top" title="Ver informações completas">
                                              Abrir
                                            </a>
                                          </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                          Paulo Alencar Assinto
                                        </td>
                                        <td class="text-center">
                                          <label class="text-justify">
                                            <a class="px-3" href="<?= BASE_DIR ?>./dados_entregador" data-toggle="tooltip" data-placement="top" title="Ver informações completas">
                                              Abrir
                                            </a>
                                          </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                          Rodrigo das Neves 
                                        </td>
                                        <td class="text-center">
                                          <label class="text-justify">
                                            <a class="px-3" href="<?= BASE_DIR ?>./dados_entregador" data-toggle="tooltip" data-placement="top" title="Ver informações completas">
                                              Abrir
                                            </a>
                                          </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                          Carlos Eduardo 
                                        </td>
                                        <td class="text-center">
                                          <label class="text-justify">
                                            <a class="px-3" href="<?= BASE_DIR ?>./dados_entregador" data-toggle="tooltip" data-placement="top" title="Ver informações completas">
                                              Abrir
                                            </a>
                                          </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                          José Pinheiro da Silva Gomes
                                        </td>
                                        <td class="text-center">
                                          <label class="text-justify">
                                            <a class="px-3" href="<?= BASE_DIR ?>./dados_entregador" data-toggle="tooltip" data-placement="top" title="Ver informações completas">
                                              Abrir
                                            </a>
                                          </label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    <!--Fim da Tabela de Produtos-->
                </span>
            </div>
        </div>
    </div>
</div>
