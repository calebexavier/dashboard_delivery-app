<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Rocket Delivery | Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--Favicon Icon-->
  <link rel="icon" type="imagem/png" href="<?= BASE_DIR ?>./dist/img/RocketLogo.png" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= BASE_DIR ?>./plugins/fontawesome-free/css/all.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= BASE_DIR ?>./plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= BASE_DIR ?>./plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Google Font: Source Sans Pro-->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
</head>
<body class="hold-transition sidebar-mini layout-fixed style">
  <div class="wrapper col-sm-4 col-lg-12 px-2 pt-5">
    <div class="col-8 col-sm-8 col-lg-8 pt-5 mt-5 wrapper fadeInDown">
      <div class="row" id="formContent">
        <div class="col-6 col-sm-6 col-lg-6 border-right">
          <div class="fadeIn first d-flex justify-content-center ml-5">
            <img src="<?= BASE_DIR ?>./dist/img/RLogo.png" id="icon" alt="User Icon" />
          </div>
        </div>
        <div class="col-6 col-sm-6 col-lg-6">
          <form class="mt-2">
            <input type="text" id="login" class="fadeIn second" name="login" placeholder="Digite seu CPF">
            <input type="text" id="password" class="fadeIn third" name="login" placeholder="Digite sua senha">
            <div class="form-check my-3 d-flex justify-content-start ajust" >
              <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
              <label class="form-check-label" for="defaultCheck1">
                Lembrar de mim
              </label>
            </div>
            <div class="form-group">
              <input type="submit" class="fadeIn fourth" value="Entrar">
            </div>
          </form>
          <div id="formFooter">
            <a class="underlineHover" href="#">Esqueceu sua senha ?</a>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--
<div class="">
    <div >
      
    </div>
  </div> -->
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>
