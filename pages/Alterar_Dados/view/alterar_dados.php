<div class="wrapper col-sm-4 col-lg-12 px-2 ">
    <div class="card-body px-auto rounded">

        <div class="col-12 col-sm-12 col-lg-12 ">
          <div class="card px-1">
            <span class="card-body">
                <h1 class="title">Alterar Dados Cadastrais</h1>
                <hr class="border-top mb-3"/>
                <br>
                    <!--Inicio do form-->
                        <form>
                            <div class="form-row">
                                <div class="col-sm-12 py-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Nome da Empresa</span>
                                        <input type="text" class="form-control" placeholder="Nome da Empresa">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-sm-6 py-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">CNPJ/CPF</span>
                                        <input type="number" class="form-control" placeholder="15.591.351/0001-91" disabled>
                                    </div>
                                </div>
                                <div class="col-sm-6 py-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Telefone</span>
                                        <input type="tel" id="telefone" class="form-control" placeholder="Telefone..." pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">
                                            <i class="fas fa-plus-circle"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <table class="table">
                                          <tbody>
                                            <tr>
                                              <td>
                                              </td>
                                              <td class="text-center">
                                                  <i class="fas fa-phone-square-alt"></i>
                                                  Telefone:
                                              </td>
                                              <td class="text-center">
                                                  (62) 999999999
                                              </td>
                                              <td class="text-center">
                                                <a href="#">
                                                  <i class="far fa-trash-alt"></i>
                                                </a>
                                              </td>
                                              <td>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>
                                              </td>
                                              <td class="text-center">
                                                  <i class="fas fa-phone-square-alt"></i>
                                                  Telefone:
                                              </td>
                                              <td class="text-center">
                                                  (62) 999999999
                                              </td>
                                              <td class="text-center">
                                                <a href="#">
                                                  <i class="far fa-trash-alt"></i>
                                                </a>
                                              </td>
                                              <td>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>
                                              </td>
                                              <td class="text-center">
                                                  <i class="fas fa-phone-square-alt"></i>
                                                  Telefone:
                                              </td>
                                              <td class="text-center">
                                                  (62) 999999999
                                              </td>
                                              <td class="text-center">
                                                <a href="#">
                                                  <i class="far fa-trash-alt"></i>
                                                </a>
                                              </td>
                                              <td>
                                              </td>
                                            </tr>
                                          </tbody>
                            </table>
                              <div class="form-row">
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">CEP</span>
                                        <input type="text" id="cep" class="form-control" placeholder="CEP">
                                      </div>
                                  </div>
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Logradouro</span>
                                        <input type="text" id="logradouro" class="form-control" placeholder="Logradouro">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-row">
                                <div class="col-sm-6 py-2">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm">Bairro</span>
                                      <input type="text" id="bairro" class="form-control" placeholder="Bairro">
                                  </div>
                                </div>
                                <div class="col-sm-3 py-2">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm">Nº</span>
                                    <input type="text" class="form-control" placeholder="Nº">
                                  </div>
                                </div>
                                <div class="col-sm-3 py-2">
                                  <div class="mt-2 ml-2">
                                    <span class="input-group-addon">
                                      <input type="checkbox" aria-label="...">
                                      <b class="text-justify">Sem número ?</b>
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div class="form-row">
                                <div class="col-sm-6 py-2">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm">Complemento</span>
                                    <input type="text" id="complemento" class="form-control" placeholder="Complemento">
                                  </div>
                                </div>
                                <div class="col-sm-4 py-2">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm">Cidade</span>
                                    <input type="text" id="localidade" class="form-control" placeholder="Cidade">
                                  </div>
                                </div>
                                <div class="col-sm-2 py-2">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm">UF</span>
                                    <input type="text" id="uf" class="form-control" placeholder="UF">
                                  </div>
                                </div>
                              </div>
                            <div class="form-row">
                                <div class="col mt-5 d-flex justify-content-end">
                                    <div id="btn-salvar" class="input-group-prepend">
                                        <button type="button" class="btn btn-primary float-right">Salvar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    <!--Fim do form-->
                </span>
            </div>
        </div>
    </div>
</div>
