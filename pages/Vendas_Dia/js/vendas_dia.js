var ctx = document.getElementsByClassName("bar-chart");

var chartGraph = new Chart(ctx, {
  type: 'bar',
  data: {
    labels:  ["00:00", "02:00", "04:00", "06:00", "08:00", "10:00", "12:00", "14:00", "16:00", "18:00", "20:00", "22:00"],
    datasets: [{
      label: "Valores Por Venda",
      data: [25,20,18,50,75,15,20,13,18,8,11,17,23],
      backgroundColor: 'rgba(77,166,253,0.85)',
      },
      {
        label: "Lucros Por Venda",
        data: [15,10,8,25,45,10,10,7,9,7,9,13,13],
        //borderWidth: 3,
        //borderColor: 'rgba(6,4,253,0.85)',
        backgroundColor: 'rgba(6,4,253,0.85)',
      },
    ]
  },
  options: {
    title: {
      display: true,
      fontSize: 20,
      text: "Relatório de Ganho Diário",
    },
    labels: {
      fontStyle: "bold",
    },
  },



});
