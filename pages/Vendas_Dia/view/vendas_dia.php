<div class="wrapper col-sm-4 col-lg-12 px-2 ">
  <div class="card-body px-auto rounded">
    <div class="col-12 col-sm-12 col-lg-12 ">
      <div class="card px-1">
          <span class="card-body">
          <h1 class="title">Receita Diária</h1>
          <hr class="border-top mb-3"/>
          <br>
          <canvas class="bar-chart"></canvas>
        </span>
      </div>
    </div>
  </div>
</div>
