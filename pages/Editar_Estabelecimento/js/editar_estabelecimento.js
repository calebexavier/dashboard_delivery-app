$("#cep-1").blur(function(){
	var cep = this.value.replace(/[^0-9]/, "");
		if(cep.length != 8){
			return false;
		}
		var url = "https://viacep.com.br/ws/"+cep+"/json/";
		$.getJSON(url, function(dadosRetorno){
		try{
			$("#logradouro").val(dadosRetorno.logradouro);
      $("#bairro").val(dadosRetorno.bairro);
			$("#complemento").val(dadosRetorno.complemento);
			$("#localidade").val(dadosRetorno.localidade);
			$("#uf").val(dadosRetorno.uf);
		}catch(ex){}
	});
});
$("#cep-2").blur(function(){
	var cep = this.value.replace(/[^0-9]/, "");
		if(cep.length != 8){
			return false;
		}
		var url = "https://viacep.com.br/ws/"+cep+"/json/";
		$.getJSON(url, function(dadosRetorno){
		try{
			$("#logradouro-2").val(dadosRetorno.logradouro);
      $("#bairro-2").val(dadosRetorno.bairro);
			$("#complemento-2").val(dadosRetorno.complemento);
			$("#localidade-2").val(dadosRetorno.localidade);
			$("#uf-2").val(dadosRetorno.uf);
		}catch(ex){}
	});
});

$(document).ready(function() {
  $("#show_hide_password a").on('click', function(event) {
      event.preventDefault();
      if($('#show_hide_password input').attr("type") == "text"){
          $('#show_hide_password input').attr('type', 'password');
          $('#show_hide_password i').addClass( "fa-eye-slash" );
          $('#show_hide_password i').removeClass( "fa-eye" );
      }else if($('#show_hide_password input').attr("type") == "password"){
          $('#show_hide_password input').attr('type', 'text');
          $('#show_hide_password i').removeClass( "fa-eye-slash" );
          $('#show_hide_password i').addClass( "fa-eye" );
      }
  });
});

/*Checkbox Sem numero - CPF*/
$(".checkbox_1").on('click',function(){
  if(document.getElementById('checkbox_1').checked){
    $("#input_1").hide(1000);
  } else {
    $("#input_1").show(1000);
  }
}); 

/*Checkbox Sem numero - CPF*/

/*Checkbox Sem numero - CNPJ*/
$(".checkbox_2").on('click',function(){
    if(document.getElementById('checkbox').checked){
      $("#input").hide(1000);
    } else {
      $("#input").show(1000);
    }
}); /*Checkbox Sem numero - CNPJ*/

$("#telefone-1").mask("(00) 0 0000-0000");
$("#telefone-2").mask("(00) 0 0000-0000");

$("#CNPJ").mask("00.000.000/0000-00");

$("#CPF").mask("000.000.000-00");

$("#CPF2").mask("000.000.000-00");

$('input:empty, textarea:empty').closest('label').addClass('empty');
