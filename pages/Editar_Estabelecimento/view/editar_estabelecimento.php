<div class="wrapper col-sm-4 col-lg-12 px-2 ">
    <div class="card-body px-auto rounded">

        <div class="col-12 col-sm-12 col-lg-12 ">
          <div class="card px-1">
            <span class="card-body">
                <h1 class="title">Cadastrar Estabelecimento</h1>
                <hr class="border-top mb-3"/>
                <br>
                    <!--Inicio do form-->
                    <div class="col-sm-12">
                      <div class="col-sm-12 mb-5">
                        <div class="list-group opt-group" id="list-tab" role="tablist">
                          <a class="list-group-item list-group-item-action active" id="pessoa-fisica-opt" data-toggle="list" href="#pessoa-fisica" role="tab" aria-controls="pessoa-fisica">Pessoa Física</a>
                          <a class="list-group-item list-group-item-action" id="pessoa-juridica-opt" data-toggle="list" href="#pessoa-juridica" role="tab" aria-controls="pessoa-juridica">Pessoa Jurídica</a>
                        </div>
                      </div>
                      <hr class="border-top mb-3"/>
                      <div class="col-sm-12">
                        <div class="tab-content" id="nav-tabContent">
                          <div class="tab-pane fade show active" id="pessoa-fisica" role="tabpanel" aria-labelledby="pessoa-fisica-opt">
                            <form> <!--Pessoa Física-->
                              <div class="form-row">
                                  <div class="col-sm-12 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">Nome do estabelecimento</span>
                                          <input type="text" class="form-control" placeholder="Nome do estabelecimento">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-row">
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">Nome</span>
                                          <input type="text" class="form-control" placeholder="Nome do proprietário">
                                      </div>
                                  </div>
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">CPF</span>
                                          <input type="text" id="CPF" class="form-control" placeholder="CPF">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-row">
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">
                                            <i class="far fa-envelope"></i>
                                          </span>
                                          <input type="text" id="email" class="form-control" placeholder="email@mail.com">
                                      </div>
                                  </div>
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">Telefone</span>
                                          <input type="tel" id="telefone-1" class="form-control" placeholder="Telefone..." pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">
                                              <i class="fas fa-plus-circle"></i>
                                          </span>
                                      </div>
                                  </div>
                              </div>
                              <table class="table">
                                            <tbody>
                                              <tr>
                                                <td>
                                                </td>
                                                <td class="text-center">
                                                    <i class="fas fa-phone-square-alt"></i>
                                                    Telefone:
                                                </td>
                                                <td class="text-center">
                                                    (62) 999999999
                                                </td>
                                                <td class="text-center">
                                                  <a href="#">
                                                    <i class="far fa-trash-alt"></i>
                                                  </a>
                                                </td>
                                                <td>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                </td>
                                                <td class="text-center">
                                                    <i class="fas fa-phone-square-alt"></i>
                                                    Telefone:
                                                </td>
                                                <td class="text-center">
                                                    (62) 999999999
                                                </td>
                                                <td class="text-center">
                                                  <a href="#">
                                                    <i class="far fa-trash-alt"></i>
                                                  </a>
                                                </td>
                                                <td>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                </td>
                                                <td class="text-center">
                                                    <i class="fas fa-phone-square-alt"></i>
                                                    Telefone:
                                                </td>
                                                <td class="text-center">
                                                    (62) 999999999
                                                </td>
                                                <td class="text-center">
                                                  <a href="#">
                                                    <i class="far fa-trash-alt"></i>
                                                  </a>
                                                </td>
                                                <td>
                                                </td>
                                              </tr>
                                            </tbody>
                              </table>
                                <div class="form-row">
                                    <div class="col-sm-6 py-2">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">CEP</span>
                                          <input type="text" id="cep-1" class="form-control" placeholder="CEP">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 py-2">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">Logradouro</span>
                                          <input type="text" id="logradouro" class="form-control" placeholder="Logradouro">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                  <div class="col-sm-6 py-2">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroup-sizing-sm">Bairro</span>
                                        <input type="text" id="bairro" class="form-control" placeholder="Bairro">
                                    </div>
                                  </div>
                                  <div class="col-sm-3 py-2" id="input_1">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroup-sizing-sm">Nº</span>
                                      <input type="text" class="form-control" placeholder="Nº">
                                    </div>
                                  </div>
                                  <div class="col-sm-3 py-2">
                                    <div class="mt-2 ml-2">
                                      <span class="input-group-addon">
                                        <input type="checkbox" class="checkbox_1" id="checkbox_1" aria-label="...">
                                        <b class="text-justify">Sem número ?</b>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-row">
                                  <div class="col-sm-6 py-2">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroup-sizing-sm">Complemento</span>
                                      <input type="text" id="complemento" class="form-control" placeholder="Complemento">
                                    </div>
                                  </div>
                                  <div class="col-sm-4 py-2">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroup-sizing-sm">Cidade</span>
                                      <input type="text" id="localidade" class="form-control" placeholder="Cidade">
                                    </div>
                                  </div>
                                  <div class="col-sm-2 py-2">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroup-sizing-sm">UF</span>
                                      <input type="text" id="uf" class="form-control" placeholder="UF">
                                    </div>
                                  </div>
                                </div>
                              <div class="form-row">
                                  <div class="col mt-5 d-flex justify-content-end">
                                    <a href="<?= BASE_DIR ?>./listar_estabelecimento">
                                      <div id="btn-salvar" class="input-group-prepend">
                                          <button type="button" class="btn btn-primary float-right">Salvar</button>
                                      </div>
                                    </a>
                                  </div>
                              </div>
                            </form>
                          </div>
                          <div class="tab-pane fade" id="pessoa-juridica" role="tabpanel" aria-labelledby="pessoa-juridica-opt">
                            <form> <!-- Pessoa Juridica -->
                              <div class="form-row">
                                  <div class="col-sm-12 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">Razão Social</span>
                                          <input type="text" class="form-control" placeholder="Razão Social">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-row">
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">Nome Fantasia</span>
                                          <input type="text" class="form-control" placeholder="Nome Fantasia">
                                      </div>
                                  </div>
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">CNPJ</span>
                                          <input type="text" id="CNPJ" class="form-control" placeholder="CNPJ">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-row">
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">Proprietário</span>
                                          <input type="text" class="form-control" placeholder="Nome do proprietário">
                                      </div>
                                  </div>
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">CPF</span>
                                          <input type="text" id="CPF2" class="form-control" placeholder="CPF">
                                      </div>
                                  </div>
                              </div>
                              <div class="form-row">
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">
                                            <i class="far fa-envelope"></i>
                                          </span>
                                          <input type="text" id="email" class="form-control" placeholder="email@mail.com">
                                      </div>
                                  </div>
                                  <div class="col-sm-6 py-2">
                                      <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">Telefone</span>
                                          <input type="tel" id="telefone-2" class="form-control" placeholder="Telefone..." pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">
                                              <i class="fas fa-plus-circle"></i>
                                          </span>
                                      </div>
                                  </div>
                              </div>
                              <table class="table">
                                            <tbody>
                                              <tr>
                                                <td>
                                                </td>
                                                <td class="text-center">
                                                    <i class="fas fa-phone-square-alt"></i>
                                                    Telefone:
                                                </td>
                                                <td class="text-center">
                                                    (62) 999999999
                                                </td>
                                                <td class="text-center">
                                                  <a href="#">
                                                    <i class="far fa-trash-alt"></i>
                                                  </a>
                                                </td>
                                                <td>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                </td>
                                                <td class="text-center">
                                                    <i class="fas fa-phone-square-alt"></i>
                                                    Telefone:
                                                </td>
                                                <td class="text-center">
                                                    (62) 999999999
                                                </td>
                                                <td class="text-center">
                                                  <a href="#">
                                                    <i class="far fa-trash-alt"></i>
                                                  </a>
                                                </td>
                                                <td>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                </td>
                                                <td class="text-center">
                                                    <i class="fas fa-phone-square-alt"></i>
                                                    Telefone:
                                                </td>
                                                <td class="text-center">
                                                    (62) 999999999
                                                </td>
                                                <td class="text-center">
                                                  <a href="#">
                                                    <i class="far fa-trash-alt"></i>
                                                  </a>
                                                </td>
                                                <td>
                                                </td>
                                              </tr>
                                            </tbody>
                              </table>
                                <div class="form-row">
                                    <div class="col-sm-6 py-2">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">CEP</span>
                                          <input type="text" id="cep-2" class="form-control" placeholder="CEP">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 py-2">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="inputGroup-sizing-sm">Logradouro</span>
                                          <input type="text" id="logradouro-2" class="form-control" placeholder="Logradouro">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                  <div class="col-sm-6 py-2">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroup-sizing-sm">Bairro</span>
                                        <input type="text" id="bairro-2" class="form-control" placeholder="Bairro">
                                    </div>
                                  </div>
                                    <div class="col-sm-3 py-2" id="input">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Nº</span>
                                        <input type="text" class="form-control" placeholder="Nº">
                                      </div>
                                    </div>
                                    <div class="col-sm-3 py-2">
                                      <div class="mt-2 ml-2">
                                        <span class="input-group-addon">
                                          <input type="checkbox" class="checkbox_2" id="checkbox" aria-label="...">
                                          <b class="text-justify">Sem número ?</b>
                                        </span>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                  <div class="col-sm-6 py-2">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroup-sizing-sm">Complemento</span>
                                      <input type="text" id="complemento-2" class="form-control" placeholder="Complemento">
                                    </div>
                                  </div>
                                  <div class="col-sm-4 py-2">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroup-sizing-sm">Cidade</span>
                                      <input type="text" id="localidade-2" class="form-control" placeholder="Cidade">
                                    </div>
                                  </div>
                                  <div class="col-sm-2 py-2">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="inputGroup-sizing-sm">UF</span>
                                      <input type="text" id="uf-2" class="form-control" placeholder="UF">
                                    </div>
                                  </div>
                                </div>
                              
                              <div class="form-row">
                                  <div class="col mt-5 d-flex justify-content-end">
                                    <a href="<?= BASE_DIR ?>./listar_estabelecimento">
                                      <div id="btn-salvar" class="input-group-prepend">
                                          <button type="button" class="btn btn-primary float-right">Salvar</button> <!--Enviar email ao clicar em salvar, gerando senha automatica-->
                                      </div>
                                    </a>
                                  </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--Fim do form-->
                </span>
            </div>
        </div>
    </div>
</div>
