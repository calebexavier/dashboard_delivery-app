<div class="wrapper col-sm-4 col-lg-12 px-2 ">
  <div class="card-body px-auto rounded">
    <div class="col-12 col-sm-12 col-lg-12 ">
      <div class="card px-1">
        <span class="card-body">
          <h1 class="title">Dados Pessoais</h1>
          <hr class="border-top mb-3"/>
          <br>
          <!--Inicio do form-->
          <form>
            <div class="form-row"><!--TODOS OS CAMPOS DEVEM SER DESABILITADOS, AS INFORMAÇÕES SERAO ALIMENTADAS VIA API-->
              <div class="col-sm-6 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Nome Completo</span>
                  <input type="text" class="form-control" placeholder="João das Neves Almeida de Oliveira" disabled>
                </div>
              </div>
              <div class="col-sm-6 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Data de Nascimento</span>
                  <input type="text" class="form-control" placeholder="18/07/1990" disabled>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-sm-5 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">RG</span>
                  <input type="text" class="form-control" placeholder="6137909" disabled>
                </div>
              </div>
              <div class="col-sm-5 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">CPF</span>
                  <input type="text" id="CPF" class="form-control" placeholder="048.665.312-98" disabled>
                </div>
              </div>
              <div class="col-sm-2 py-2">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">
                      <a href="<?= BASE_DIR ?>./perfil">
                        <i class="fas fa-camera"></i>
                        Perfil
                      </a>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-sm-5 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">
                    <i class="far fa-envelope"></i>
                  </span>
                  <input type="text" id="email" class="form-control" placeholder="joaoneves@gmail.com" disabled>
                </div>
              </div>
              <div class="col-sm-5 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Celular</span>
                  <input type="tel" id="telefone-1" class="form-control" placeholder="(62) 9 8615-4875" disabled>
                </div>
              </div>
              <div class="col-sm-2 py-2">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">
                      <a href="<?= BASE_DIR ?>./identidade">
                        <i class="fas fa-camera"></i>
                        Identidade
                      </a>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <h1 class="title mt-5">Veículo</h1> <!--PLACA E DOCUMENTO DO VEICULO(FOTO)--->
            <hr class="border-top mb-3"/>
            <br>
            <div class="form-row">
              <div class="col-sm-4 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Categoria</span>
                  <input type="text" class="form-control" placeholder="Motocicleta" disabled>
                </div>
              </div>
              <div class="col-sm-3 py-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-sm">Placa</span>
                  <input type="text" class="form-control" placeholder="NKM-8156" disabled>
                </div>
              </div>
              <div class="col-sm-3 py-2">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">
                      <a href="<?= BASE_DIR ?>./documento_veiculo">
                        <i class="fas fa-camera mr-2"></i>
                        Documento do Veículo
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-sm-2 py-2">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">
                      <a href="<?= BASE_DIR ?>./cnh">
                        <i class="fas fa-camera mr-2"></i>
                        CNH
                      </a>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col mt-5 d-flex justify-content-end">
                <a href="<?= BASE_DIR ?>./#">
                  <div id="btn-salvar" class="input-group-prepend">
                    <button type="button" class="btn btn-success float-right">Aceitar</button> <!--Verde-->
                  </div>
                </a>
              </div>
              <div class="col mt-5 d-flex justify-content-start">
                <a href="<?= BASE_DIR ?>./#">
                  <div id="btn-salvar" class="input-group-prepend">
                    <button type="button" class="btn btn-danger float-right">Recusar</button> <!--Vermelho-->
                  </div>
                </a>
              </div>
            </div>
          </form><!--Fim do form-->
        </span>
      </div>
    </div>
  </div>
</div>
