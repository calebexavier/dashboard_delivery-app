<div class="wrapper col-sm-4 col-lg-12 px-2 ">
  <div class="card-body px-auto rounded">
    <div class="col-12 col-sm-12 col-lg-12 ">
      <div class="card px-1">
          <span class="card-body">
          <h1 class="title">Gráficos</h1>
          <hr class="border-top mb-3"/>
          <br>
          <div class="row">
            <div class="col-12 col-sm-12 col-lg-12">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>R$ 1200,00 </h3>

                  <p>Receita Diária 28/10/2020</p>
                </div>
                <div class="icon">
                  <i class="ion ion-cash"></i>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-12 col-lg-12">
              <canvas class="bar-chart"></canvas> <!--GRÁFICO EM BARRA, HORIZONTAL DIA DO MÊS, NA VERTICAL O VALOR, BARRAS VERDE -->
            </div>
          </div>
        </span>
      </div>
    </div>
  </div>
</div>
