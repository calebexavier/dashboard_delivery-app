var ctx = document.getElementsByClassName("bar-chart");


var chartGraph = new Chart(ctx, {
  type: 'bar',
  data: {
    labels:  [
      "Dia: 01", 
      "Dia: 02", 
      "Dia: 03", 
      "Dia: 04", 
      "Dia: 05", 
      "Dia: 06", 
      "Dia: 07", 
      "Dia: 08", 
      "Dia: 09", 
      "Dia: 10", 
      "Dia: 11", 
      "Dia: 12", 
      "Dia: 13", 
      "Dia: 14", 
      "Dia: 15", 
      "Dia: 16", 
      "Dia: 17", 
      "Dia: 18", 
      "Dia: 19", 
      "Dia: 20", 
      "Dia: 21", 
      "Dia: 22", 
      "Dia: 23", 
      "Dia: 24", 
      "Dia: 25", 
      "Dia: 26", 
      "Dia: 27", 
      "Dia: 28", 
      "Dia: 29", 
      "Dia: 30"
    ],
    datasets: [{
      label: "Ganho por dia",
      data: [
        548.65,
        305.50,
        289.65,
        301,
        402,
        156,
        500,
        282.65,
        365,
        207,
        451.65,
        894,
        360,
        214.00,
        205.50,
        189.65,
        201,
        259.68,
        203.25,
        604,
        505,
        206,
        307,
        295,
        894,
        180,
        214.00,
        1200,
        0,
        0
      ],
      backgroundColor: 'rgba(10,194,130,0.85)',
    }],
  },
  options: {
    title: {
      display: true,
      fontSize: 20,
      text: "Relatório de Ganho Mensal",
    },
    labels: {
      fontStyle: "bold",
    },
  },
});

var ctz = document.getElementsByClassName("pizza-chart-2");

var chartGraph = new Chart(ctz, {
  type: 'pie',
  data: {
    labels:  ["00:00", "02:00", "04:00", "06:00", "08:00", "10:00", "12:00", "14:00", "16:00", "18:00", "20:00", "22:00"],
    datasets: [{
      label: "Valores Por Venda",
      data: [25.60,32.50,18.20,50.25,27.30,15.45,20.10,13.08,18.20,8.50,11.30,17.99],
      backgroundColor: [
        'rgba(63,191,63,0.85)',
        'rgba(235,229,45,0.85)',
        'rgba(235,45,45,0.85)',
        'rgba(63,191,63,0.85)',
        'rgba(235,229,45,0.85)',
        'rgba(235,45,45,0.85)',
        'rgba(63,191,63,0.85)',
        'rgba(235,229,45,0.85)',
        'rgba(235,45,45,0.85)',
        'rgba(63,191,63,0.85)',
        'rgba(235,229,45,0.85)',
        'rgba(235,45,45,0.85)',
        ]
      },
    ]
  },
  options: {
    title: {
      display: true,
      fontSize: 20,
      text: "Relatório de Ganho Diário",
    },
    labels: {
      fontStyle: "bold",
    },
    layout: {
      padding: {
          left: 0,
          right: 0,
          top: 50,
          bottom: 0
      }
  }
  },



});
a
