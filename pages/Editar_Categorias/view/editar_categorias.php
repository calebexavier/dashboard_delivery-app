<div class="wrapper col-sm-4 col-lg-12 px-2 ">
    <div class="card-body px-auto rounded">

        <div class="col-12 col-sm-12 col-lg-12 ">
          <div class="card px-1">
            <span class="card-body">
                <h1 class="title">Editar / Cadastrar Categorias</h1>
                <hr class="border-top mb-3"/>
                <br>
                    <!--Inicio do form-->
                        <form>
                            <div class="form-row">
                                <div class="col-sm-6 py-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Categoria do Produto</span>
                                        <input type="text" class="form-control" placeholder="Nome da Categoria">
                                    </div>
                                </div>
                                <div class="col-sm-6 py-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Código da Categoria</span>
                                        <input type="number" class="form-control" placeholder="Codigo">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-sm-12 py-2">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" id="inputGroup-sizing-sm">Descrição</label>
                                        <textarea type="text" class="form-control" placeholder="Descrição..." rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col ml-1 mt-3">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1">Ativar categoria</label>
                                    </div>
                                </div>

                                <div class="col ml-3 mt-3">
                                    <div id="btn-salvar" class="input-group-prepend text-right">
                                        <button type="button" class="btn btn-primary float-right">Salvar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    <!--Fim do form-->
                </span>
            </div>
        </div>
    </div>
</div>
