<div class="wrapper col-sm-4 col-lg-12 px-2 ">
    <div class="card-body px-auto rounded">

        <div class="col-12 col-sm-12 col-lg-12 ">
          <div class="card px-1">
            <span class="card-body">
              <h1 class="title">Alterar Senha</h1>
              <hr class="border-top mb-3"/>
              <br>
              <!--Inicio do form-->
              <form class="d-flex justify-content-center">
              <div class="col-sm-6">
                <div class="form-group">
                  <label>Senha Nova</label>
                  <div class="input-group" id="show_hide_password_1">
                    <input type="password" class="form-control" placeholder="Senha Nova">
                      <div class="input-group-append">
                        <a class="btn btn-outline-primary" type="button" id="button-addon2">
                          <i class="fa fa-eye-slash" aria-hidden="true"></i>
                        </a>
                      </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Repetir Senha Nova</label>
                    <div class="input-group" id="show_hide_password_2">
                      <input type="password" class="form-control" placeholder="Repita a Senha Nova">
                      <div class="input-group-append">
                        <a class="btn btn-outline-primary" type="button" id="button-addon2">
                          <i class="fa fa-eye-slash" aria-hidden="true"></i>
                        </a>
                      </div>
                    </div>
                </div>
                <div id="btn-salvar" class="input-group-prepend justify-content-center mt-3">
                  <button type="submit" class="btn btn-primary" href="<?= BASE_DIR ?>./home">Salvar</button>
                </div>
              </div>
              </form>
            <!--Fim do form-->
            </span>
            </div>
        </div>
    </div>
</div>
