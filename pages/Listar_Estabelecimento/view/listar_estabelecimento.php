<div class="wrapper col-sm-4 col-lg-12 px-2 ">
    <div class="card-body px-auto rounded">

        <div class="col-12 col-sm-12 col-lg-12 ">
          <div class="card px-1">
            <span class="card-body">
                <h1 class="title">Lista de Estabelecimentos</h1>
                <hr class="border-top mb-3"/>
                <br>
                <br>
                    <!--Campo de pesquisa-->
                        <div class="form-group mx-3">
                            <input class="form-control" id="campo_pesquisa_tabela" type="text" placeholder="Pesquisar por...">
                        </div>
                    <!--Fim do Campo de pesquisa-->

                    <!--Inicio da tabela de Produtos-->
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="bg-danger">
                                    <th class="text-center border-left">Nome do Estabelecimento</th>
                                    <th class="text-center border-left">CNPJ/CPF</th>
                                    <th class="text-center border-left">CEP</th>
                                    <th class="text-center border-left">Status</th>
                                    <th class="text-center border-left">Função</th>
                                </thead>
                                <tbody id="tabela_categorias_corpo">
                                    <tr>
                                        <td class="text-center">
                                          Hamburgueria Gerson
                                        </td>
                                        <td class="text-center">
                                          15.591.351/0001-91
                                        </td>
                                        <td class="text-center">
                                            74355420
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                          <a class="px-3" href="<?= BASE_DIR ?>./#" data-toggle="tooltip" data-placement="top" title="Editar Cadastro do Estabelecimento">
                                            <i class="far fa-edit"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                          Hamburgueria Gerson
                                        </td>
                                        <td class="text-center">
                                          15.591.351/0001-91
                                        </td>
                                        <td class="text-center">
                                            74355420
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                          <a class="px-3" href="<?= BASE_DIR ?>./#" data-toggle="tooltip" data-placement="top" title="Editar Cadastro do Estabelecimento">
                                            <i class="far fa-edit"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                          Hamburgueria Gerson
                                        </td>
                                        <td class="text-center">
                                          15.591.351/0001-91
                                        </td>
                                        <td class="text-center">
                                            74355420
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                          <a class="px-3" href="<?= BASE_DIR ?>./#" data-toggle="tooltip" data-placement="top" title="Editar Cadastro do Estabelecimento">
                                            <i class="far fa-edit"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                          Hamburgueria Gerson
                                        </td>
                                        <td class="text-center">
                                          15.591.351/0001-91
                                        </td>
                                        <td class="text-center">
                                            74355420
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                          <a class="px-3" href="<?= BASE_DIR ?>./#" data-toggle="tooltip" data-placement="top" title="Editar Cadastro do Estabelecimento">
                                            <i class="far fa-edit"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                          Hamburgueria Gerson
                                        </td>
                                        <td class="text-center">
                                          15.591.351/0001-91
                                        </td>
                                        <td class="text-center">
                                            74355420
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                          <a class="px-3" href="<?= BASE_DIR ?>./#" data-toggle="tooltip" data-placement="top" title="Editar Cadastro do Estabelecimento">
                                            <i class="far fa-edit"></i>
                                          </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                          Hamburgueria Gerson
                                        </td>
                                        <td class="text-center">
                                          15.591.351/0001-91
                                        </td>
                                        <td class="text-center">
                                            74355420
                                        </td>
                                        <td class="text-center">
                                            Ativo
                                        </td>
                                        <td class="text-center">
                                          <a class="px-3" href="<?= BASE_DIR ?>./#" data-toggle="tooltip" data-placement="top" title="Editar Cadastro do Estabelecimento">
                                            <i class="far fa-edit"></i>
                                          </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    <!--Fim da Tabela de Produtos-->
                </span>
            </div>
        </div>
    </div>
</div>
