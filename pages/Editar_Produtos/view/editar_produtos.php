<div class="wrapper col-sm-4 col-lg-12 px-2 ">
    <div class="card-body px-auto rounded">

        <div class="col-12 col-sm-12 col-lg-12 ">
          <div class="card px-1">
            <span class="card-body">
                <h1 class="title">Cadastrar Produtos</h1>
                <hr class="border-top mb-3"/>
                <br>
                    <!--Inicio do form-->
                        <form>
                            <div class="form-row">
                                <div class="col-sm-6 py-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Nome do Produto</span>
                                        <input type="text" class="form-control" placeholder="Nome do Produto">
                                    </div>
                                </div>
                                <div class="col-sm-6 py-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm" for="dinheiro">R$</span>
                                        <input type="text" class="dinheiro form-control" placeholder="0,00">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-sm-6 py-2">
                                    <div class="form-group input-group-prepend">
                                      <span class="input-group-text" id="inputGroup-sizing-sm">Categoria</span>
                                      <select class="form-control" id="exampleFormControlSelect1">
                                        <option>Grelhados</option>
                                        <option>Porções</option>
                                        <option>Sanduíche</option>
                                        <option>Sobremesas</option>
                                        <option>Bebidas</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 py-2">
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupFileAddon01">
                                    <i class="fas fa-camera mr-2"></i>
                                      Foto</span>
                                  </div>
                                  <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                    <label class="custom-file-label" for="inputGroupFile01">Escolher Arquivo ...</label>
                                  </div>
                                </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-sm-12 py-2">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" id="inputGroup-sizing-sm">Descrição</label>
                                        <textarea type="text" class="form-control" placeholder="Descrição..." rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col ml-1 mt-3">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="customSwitch1">
                                        <label class="custom-control-label" for="customSwitch1">Ativar produto</label>
                                    </div>
                                </div>

                                <div class="col ml-3 mt-3">
                                    <div id="btn-salvar" class="input-group-prepend text-right">
                                        <a href="<?= BASE_DIR ?>./listar_produtos">
                                          <button type="button" class="btn btn-primary float-right">Salvar</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    <!--Fim do form-->
                </span>
            </div>
        </div>
    </div>
</div>
